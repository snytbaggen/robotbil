# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/pi/robotbil/libs/raspicam_cv/RaspiCamCV.c" "/home/pi/robotbil/CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.o"
  "/home/pi/robotbil/libs/robotnetwork.c" "/home/pi/robotbil/CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.o"
  "/home/pi/robotbil/libs/robotserial.c" "/home/pi/robotbil/CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "/opt/vc/include"
  "/home/pi/git/raspberrypi/userland"
  "/home/pi/git/raspberrypi/userland/host_applications/linux/libs/bcm_host/include"
  "/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam"
  "/home/pi/git/raspberrypi/userland/interface/vcos"
  "/home/pi/git/raspberrypi/userland/interface/vcos/pthreads"
  "/home/pi/git/raspberrypi/userland/interface/vmcs_host/linux"
  "/home/pi/git/raspberrypi/userland/interface/khronos/include"
  "/home/pi/git/raspberrypi/userland/interface/khronos/common"
  "/home/pi/git/robidouille/raspicam_cv"
  "libs/raspicam_cv"
  "libs"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
