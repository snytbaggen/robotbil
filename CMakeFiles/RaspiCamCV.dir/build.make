# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/pi/robotbil

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/pi/robotbil

# Include any dependencies generated for this target.
include CMakeFiles/RaspiCamCV.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/RaspiCamCV.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/RaspiCamCV.dir/flags.make

CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.o: CMakeFiles/RaspiCamCV.dir/flags.make
CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.o: libs/raspicam_cv/RaspiCamCV.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/pi/robotbil/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.o"
	/usr/bin/gcc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.o   -c /home/pi/robotbil/libs/raspicam_cv/RaspiCamCV.c

CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.i"
	/usr/bin/gcc  $(C_DEFINES) $(C_FLAGS) -E /home/pi/robotbil/libs/raspicam_cv/RaspiCamCV.c > CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.i

CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.s"
	/usr/bin/gcc  $(C_DEFINES) $(C_FLAGS) -S /home/pi/robotbil/libs/raspicam_cv/RaspiCamCV.c -o CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.s

CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.o.requires:
.PHONY : CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.o.requires

CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.o.provides: CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.o.requires
	$(MAKE) -f CMakeFiles/RaspiCamCV.dir/build.make CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.o.provides.build
.PHONY : CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.o.provides

CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.o.provides.build: CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.o

CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.o: CMakeFiles/RaspiCamCV.dir/flags.make
CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.o: libs/robotnetwork.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/pi/robotbil/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.o"
	/usr/bin/gcc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.o   -c /home/pi/robotbil/libs/robotnetwork.c

CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.i"
	/usr/bin/gcc  $(C_DEFINES) $(C_FLAGS) -E /home/pi/robotbil/libs/robotnetwork.c > CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.i

CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.s"
	/usr/bin/gcc  $(C_DEFINES) $(C_FLAGS) -S /home/pi/robotbil/libs/robotnetwork.c -o CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.s

CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.o.requires:
.PHONY : CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.o.requires

CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.o.provides: CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.o.requires
	$(MAKE) -f CMakeFiles/RaspiCamCV.dir/build.make CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.o.provides.build
.PHONY : CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.o.provides

CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.o.provides.build: CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.o

CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.o: CMakeFiles/RaspiCamCV.dir/flags.make
CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.o: libs/robotserial.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/pi/robotbil/CMakeFiles $(CMAKE_PROGRESS_3)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.o"
	/usr/bin/gcc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.o   -c /home/pi/robotbil/libs/robotserial.c

CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.i"
	/usr/bin/gcc  $(C_DEFINES) $(C_FLAGS) -E /home/pi/robotbil/libs/robotserial.c > CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.i

CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.s"
	/usr/bin/gcc  $(C_DEFINES) $(C_FLAGS) -S /home/pi/robotbil/libs/robotserial.c -o CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.s

CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.o.requires:
.PHONY : CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.o.requires

CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.o.provides: CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.o.requires
	$(MAKE) -f CMakeFiles/RaspiCamCV.dir/build.make CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.o.provides.build
.PHONY : CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.o.provides

CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.o.provides.build: CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.o

# Object files for target RaspiCamCV
RaspiCamCV_OBJECTS = \
"CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.o" \
"CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.o" \
"CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.o"

# External object files for target RaspiCamCV
RaspiCamCV_EXTERNAL_OBJECTS =

libRaspiCamCV.a: CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.o
libRaspiCamCV.a: CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.o
libRaspiCamCV.a: CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.o
libRaspiCamCV.a: CMakeFiles/RaspiCamCV.dir/build.make
libRaspiCamCV.a: CMakeFiles/RaspiCamCV.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking C static library libRaspiCamCV.a"
	$(CMAKE_COMMAND) -P CMakeFiles/RaspiCamCV.dir/cmake_clean_target.cmake
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/RaspiCamCV.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/RaspiCamCV.dir/build: libRaspiCamCV.a
.PHONY : CMakeFiles/RaspiCamCV.dir/build

CMakeFiles/RaspiCamCV.dir/requires: CMakeFiles/RaspiCamCV.dir/libs/raspicam_cv/RaspiCamCV.c.o.requires
CMakeFiles/RaspiCamCV.dir/requires: CMakeFiles/RaspiCamCV.dir/libs/robotnetwork.c.o.requires
CMakeFiles/RaspiCamCV.dir/requires: CMakeFiles/RaspiCamCV.dir/libs/robotserial.c.o.requires
.PHONY : CMakeFiles/RaspiCamCV.dir/requires

CMakeFiles/RaspiCamCV.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/RaspiCamCV.dir/cmake_clean.cmake
.PHONY : CMakeFiles/RaspiCamCV.dir/clean

CMakeFiles/RaspiCamCV.dir/depend:
	cd /home/pi/robotbil && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/pi/robotbil /home/pi/robotbil /home/pi/robotbil /home/pi/robotbil /home/pi/robotbil/CMakeFiles/RaspiCamCV.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/RaspiCamCV.dir/depend

