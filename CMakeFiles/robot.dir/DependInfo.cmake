# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/RaspiCLI.c" "/home/pi/robotbil/CMakeFiles/robot.dir/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/RaspiCLI.c.o"
  "/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/RaspiCamControl.c" "/home/pi/robotbil/CMakeFiles/robot.dir/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/RaspiCamControl.c.o"
  "/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/RaspiPreview.c" "/home/pi/robotbil/CMakeFiles/robot.dir/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/RaspiPreview.c.o"
  "/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/RaspiTex.c" "/home/pi/robotbil/CMakeFiles/robot.dir/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/RaspiTex.c.o"
  "/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/RaspiTexUtil.c" "/home/pi/robotbil/CMakeFiles/robot.dir/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/RaspiTexUtil.c.o"
  "/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/gl_scenes/mirror.c" "/home/pi/robotbil/CMakeFiles/robot.dir/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/gl_scenes/mirror.c.o"
  "/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/gl_scenes/sobel.c" "/home/pi/robotbil/CMakeFiles/robot.dir/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/gl_scenes/sobel.c.o"
  "/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/gl_scenes/square.c" "/home/pi/robotbil/CMakeFiles/robot.dir/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/gl_scenes/square.c.o"
  "/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/gl_scenes/teapot.c" "/home/pi/robotbil/CMakeFiles/robot.dir/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/gl_scenes/teapot.c.o"
  "/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/gl_scenes/yuv.c" "/home/pi/robotbil/CMakeFiles/robot.dir/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/gl_scenes/yuv.c.o"
  "/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/tga.c" "/home/pi/robotbil/CMakeFiles/robot.dir/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam/tga.c.o"
  "/home/pi/robotbil/libs/raspicam_cv/RaspiCamCV.c" "/home/pi/robotbil/CMakeFiles/robot.dir/libs/raspicam_cv/RaspiCamCV.c.o"
  "/home/pi/robotbil/libs/robotnetwork.c" "/home/pi/robotbil/CMakeFiles/robot.dir/libs/robotnetwork.c.o"
  "/home/pi/robotbil/libs/robotserial.c" "/home/pi/robotbil/CMakeFiles/robot.dir/libs/robotserial.c.o"
  "/home/pi/robotbil/main.c" "/home/pi/robotbil/CMakeFiles/robot.dir/main.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "/opt/vc/include"
  "/home/pi/git/raspberrypi/userland"
  "/home/pi/git/raspberrypi/userland/host_applications/linux/libs/bcm_host/include"
  "/home/pi/git/raspberrypi/userland/host_applications/linux/apps/raspicam"
  "/home/pi/git/raspberrypi/userland/interface/vcos"
  "/home/pi/git/raspberrypi/userland/interface/vcos/pthreads"
  "/home/pi/git/raspberrypi/userland/interface/vmcs_host/linux"
  "/home/pi/git/raspberrypi/userland/interface/khronos/include"
  "/home/pi/git/raspberrypi/userland/interface/khronos/common"
  "/home/pi/git/robidouille/raspicam_cv"
  "libs/raspicam_cv"
  "libs"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
