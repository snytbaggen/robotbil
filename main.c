#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <termios.h>
#include <time.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <unistd.h>
#include <netinet/in.h>

#include "RaspiCamCV.h"
#include "libs/robotserial.h"
#include "libs/robotnetwork.h"
//#include "libs/instruction_types.h"
#include "imgmodes.h"

/*
CvCapture -> RaspiCamCvCapture
cvCreateCameraCapture -> raspiCamCvCreateCameraCapture
cvQueryFrame -> raspiCamCvQueryFrame
cvReleaseCapture -> raspiCamCvReleaseCapture
cvGetCaptureProperty -> raspiCamCvGetCaptureProperty
cvSetCaptureProperty does not currently work. Use the raspiCamCvCreateCameraCapture2 method to specify width, height, framerate, bitrate and monochrome settings.
*/

//Function declarations
RaspiCamCvCapture* cameraInit(int width, int height);
void getPixelValuesFromPos(IplImage* img, uint8_t* buffer, int x, int y);
void MyCallbackFunc(int event, int x, int y, int flags, void* param);
void stop();
void* UDPSend(void*arg);
void filterImages();
void executeInstruction();
int instructionReceived();
void quit();

#define PORT 8888
#define WIDTH 160
#define HEIGHT 120
#define FRAMERATE 30
#define WATCHDOG_ENABLED 1
#define VIDEO_ENABLED 1

IplImage* image;
IplImage* send_image;
IplImage* tresh_image;
IplImage* hsv_image;
uint8_t* pixelvals;
int hsv_range = 7;
int recv_len;

int send_ok = 1;
int udp_send_len = BUFLEN_IMG;
int udp_img_mode = 0;

int automatic_mode = 0;

uint8_t serial_send_buffer[16];

int serial_fd;
uint8_t instruction[16];
uint8_t instruction_type;
int instruction_length;

int instructionReceived(){
  instruction_length = getMessage(instruction);
  if (instruction_length != -1 && isValidNetInstruction(instruction, instruction_length)){
    return 1;
  }
  return 0;
}

void setMoveDirection(char dir){
  char* instr;
  if (dir == DIR_FWD){
    formatInstruction(INSTR_FORWARD, NULL, instr);
    printf("forward: %d\n", instr[0]>>4);
  }
  else{
    formatInstruction(INSTR_BACK, NULL, instr);
    printf("backward: %d\n", instr[0]>>4);
  }

  sendInstruction(instr);
}

void setTurn(char dir, char speed){
  char instr[2];
  if (dir == TURN_R)
    formatInstruction(INSTR_RIGHT, &speed, instr);
  else
    formatInstruction(INSTR_LEFT, &speed, instr);

  sendInstruction(instr);  
}

void setMoveSpeed(uint8_t speed){
  char instr[2];
  formatInstruction(INSTR_SPEED, &speed, instr);
  sendInstruction(instr);
}


void executeInstruction(){
  int type = instruction[0] >> 4;
  uint8_t move, turn;
  int turn_direction;
  switch(type){
  case NETINSTR_STOP:
    printf("stop: %d\n", type);
    stop();
    break;
  case NETINSTR_CONTACT:
    printf("contact\n");
    sendMessage(NETINSTR_ACK,1);
    break;
  case NETINSTR_PING:
    //Do nothing, just acknowledge an instruction has been sent
    break;
  case NETINSTR_BTNS:
    printf("button\n");
    //TODO: update buttons
    break;
  case NETINSTR_DIR:
    //m    int forward, sideways;
    if ((int8_t)instruction[1] < 0){
      setMoveDirection(DIR_BWD);
      move = -2*(int8_t)instruction[1]-1;
    }else if ((int8_t)instruction[1] > 0){
      setMoveDirection(DIR_FWD);
      move = 2*(int8_t)instruction[1]+1;
    }else{
      move = 0;
    }

    if ((int8_t)instruction[2] < 0){
      turn_direction = TURN_L;
      turn = -1*(int8_t)instruction[2]-1;
    }else if ((int8_t)instruction[2] > 0){
      turn_direction = TURN_R;
      turn = (int8_t)instruction[2]+1;
    }else{
      turn = 0;
      turn_direction = TURN_R;
    }

    setMoveSpeed(move);
    setTurn(turn_direction, turn);

    /*    move = 2*(int8_t)instruction[1];
    turn = 2*(int8_t)instruction[2];
    
    if (move != 0)
      move++;

    if (turn != 0)
      turn++;

    if (move >= 0){
      setMoveDirection(DIR_FWD);
      setMoveSpeed(char)(move);
    }else{
      setMoveDirection(DIR_BWD);
      setMoveSpeed((char)-move);
    }

    if (turn >= 0){
      setTurnDirection(TURN_R);
      setTurnSpeed((char)turn);
    }else{
      setTurnDirection(TURN_L);
      setTurnSpeed((char)-turn);
      }*/

    printf("dir: %d %d\n",move, turn);

    //TODO: update directions
    break;
  case NETINSTR_MOUSE:
    if (hsv_image != NULL){
      printf("x, y: %d, %d\n",instruction[1],instruction[2]);
      getPixelValuesFromPos(hsv_image, pixelvals, instruction[1], instruction[2]);
    }
    break;
  case NETINSTR_IMGMODE:
    udp_img_mode = instruction[1];
  default:
    break;
  }
  fflush(stdout);
}

int main(){
  if (initNetwork(PORT) == -1){
    printf("Network socket failed to open on port %d\n", PORT);
  }else{
    printf("Network socket open on port %d\n",PORT);
  }
  fflush(stdout);

  printf("Opening serial port... ");

  serial_fd = openPort("/dev/ttyUSB0");

  if (serial_fd != -1){
    setInterfaceAttribs(serial_fd, B19200, 0);
    printf("Open.\n");
  }else{
    printf("Failed.\n");
  }

  RaspiCamCvCapture* capture = cameraInit(WIDTH,HEIGHT);

  CvFont font;
  double hScale=0.4;
  double vScale=0.4;
  int lineWidth=1;

  pixelvals = (uint8_t*)malloc(3*sizeof(uint8_t));
  pixelvals[0]=50;
  pixelvals[1]=50;
  pixelvals[2]=50;

  cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX|CV_FONT_ITALIC, hScale, vScale, 0, lineWidth, 8);

  if (VIDEO_ENABLED){
    cvNamedWindow("RaspiCamTest", 1);
    //  cvNamedWindow("RaspiCamBlur", 2);
    cvSetMouseCallback("RaspiCamTest", MyCallbackFunc, NULL);
  }
  //CvMat* conv_image

  //int exit = 0;
  //  char buf[16];

  printf("Waiting for contact... ");
  fflush(stdout);
  waitForContact();
  printf("Done.\n");
  fflush(stdout);

  image = raspiCamCvQueryFrame(capture);
  send_image = cvCloneImage(image);

  pthread_t* udpthread = malloc(sizeof(udpthread));
  pthread_create(udpthread, NULL, &UDPSend, NULL);
  
  //  send_ok = 1;
  startTimer();
  do{
    image = raspiCamCvQueryFrame(capture);
    hsv_image      = cvCreateImage(cvGetSize(image),image->depth,image->nChannels);
    tresh_image = cvCreateImage(cvGetSize(image),image->depth,1);
    filterImages();

    if (instructionReceived()){
      executeInstruction();
      resetTimer();
    }else if (hardTimeoutExceeded()){
      if (WATCHDOG_ENABLED){
	printf("Timeout exceeded!\n");
	send_ok = 0;
	stop();
	waitForContact();
	udp_img_mode = IMGMODE_BGR;
	send_ok = 1;
	resetTimer();
      }
    }
     
    //Get moments
    CvMoments* moments = (CvMoments*)malloc(sizeof(CvMoments));
    cvMoments(tresh_image, moments, 1);
    double moment10 = cvGetSpatialMoment(moments, 1, 0);
    double moment01 = cvGetSpatialMoment(moments, 0, 1);

    //Get total area and center of mass
    double area = cvGetCentralMoment(moments, 0, 0);
    int posx = (int)floor(moment10/area);
    int posy = (int)floor(moment01/area);

    //Draw circle on image
    cvCircle(image, cvPoint(posx,posy), 5, cvScalar(0,255,255,0), 1,8,0);

    //Copy image to be sent
    switch(udp_img_mode){
    case IMGMODE_BGR:
      memcpy(send_image->imageData, image->imageData, BUFLEN_IMG);
      udp_send_len = BUFLEN_IMG;
      break;
    case IMGMODE_HSV:
      memcpy(send_image->imageData, hsv_image->imageData, BUFLEN_IMG);
      udp_send_len = BUFLEN_IMG;
      break;
    case IMGMODE_FILTERED:
      memcpy(send_image->imageData, tresh_image->imageData, BUFLEN_IMG/3);
      udp_send_len = BUFLEN_IMG/3;
      break;
    default:
      break;
    }

    if (VIDEO_ENABLED)
      cvShowImage("RaspiCamTest", image);

    char key = cvWaitKey(10);
    if (key == 27) //escape
      break;
    else if (key == 82) //up
      hsv_range++;
    else if (key == 84) //down
      hsv_range--;

    cvReleaseImage(&hsv_image);
    cvReleaseImage(&tresh_image);
    free(moments);
  }while(1);

  /*CvMat frame;
  CvMat frameCopy;
  CvMat image;

  capture = (RaspiCamCvCapture*)cvCaptureFromCAM(0);

  if (!capture)
    printf("No camera detected!\n");

  cvNamedWindow("result", 1);

  if (capture){
    for (;;){
      IplImage* iplImg = cvQueryFrame((CvCapture*)capture);
      frame = iplImg;
      if (frame.empty())
	break;
      if (iplImg->origin == IPL_ORIGIN_TL)
	frame.copyTo(frameCopy);
      else
	flip(frame, frameCopy, 0);

      if (waitKey(10) >= 0)
	raspiCamCvReleaseCapture(&capture);
    }
    waitKey(0);

    cvDestroyWindow("result");
    }*/
  
  /*int fd =   open_port("/dev/ttyUSB0");

  if (fd != -1){
    set_interface_attribs(fd, B19200, 0);
  }

  uint8_t* data = (uint8_t*)malloc(sizeof(uint8_t));
  (*data)=0b01001011;*/
  
   //  printf("svar: %d\n",send_command(test,1));

  /*uint8_t test[3];
  format_instruction(4, data, test);
  //test[0] +=  1;
  printf("Sending: ");
  print_binary_instruction(test);
  uint8_t answer = send_instruction(test);
  printf("Arduino answer: ");
  print_binary_answer(answer);
  */
  quit();
  return 0;
}

void quit(){
  stop();
  closeNetwork();
  printf("Network socket closed\n");
  setRGBLed(LED_OFF,LED_OFF,LED_OFF);
}

RaspiCamCvCapture* cameraInit(int width, int height){
  RASPIVID_CONFIG* config = (RASPIVID_CONFIG*)malloc(sizeof(RASPIVID_CONFIG));

  config->width=width;
  config->height=height;
  config->bitrate=0;
  config->framerate=0;
  config->monochrome=0;

  RaspiCamCvCapture* capture = (RaspiCamCvCapture*) raspiCamCvCreateCameraCapture2(0, config);
  free(config);
  return capture;
}

void getPixelValuesFromPos(IplImage* img, uint8_t* buffer, int x, int y){

  int i, channels;
  channels = img->nChannels;
  for (i=0; i < channels; i++){
    buffer[i]=CV_IMAGE_ELEM(img, uchar, y, (x*channels)+i);
  }
}

void MyCallbackFunc(int event, int x, int y, int flags, void* param){
  switch (event){
  case CV_EVENT_LBUTTONDBLCLK:
    if (hsv_image != NULL){
      getPixelValuesFromPos(hsv_image, pixelvals, y, x);
    }
  default:
    break;
  }
}

void stop(){
  //stop robot
}

void* UDPSend(void *arg){
  clock_t start;
  int i,ms;
  while(1){
    ms=0;
    start = clock();
    if (send_ok)
      sendMessage(send_image->imageData, udp_send_len);
    while(ms < 1000/FRAMERATE)
      ms = (clock()-start)*1000/CLOCKS_PER_SEC;
  }
}

void filterImages(){
  /******************************************************************************
   *Circle detection, the code below puts the found circles in circle_storage.  *
   *However, it appears that the Pi is too weak to do this detection fast enough*
   ******************************************************************************/
  /*
    Ipl_Image* circle_image = cvCreateImage(cvGetSize(image),image->depth,1);
    cvSmooth(image, hsv_image, CV_GAUSSIAN,3,0,0,0);
    cvCvtColor(hsv_image, circle_image, CV_BGR2GRAY);
    
    CvMemStorage* circle_storage=cvCreateMemStorage(0);// = (CvMemStorage*)malloc(sizeof(CvMemStorage));
    cvHoughCircles(circle_image, circle_storage, CV_HOUGH_GRADIENT,1,filtered_image->width/8,100,100,0,0);*/
  
  cvSmooth(image, hsv_image, CV_GAUSSIAN,3,0,0,0); //Smooth image
  cvCvtColor(image, hsv_image, CV_BGR2HSV); //Convert from BGR to HSV
  
  cvInRangeS(hsv_image, 
	     cvScalar(pixelvals[0]-hsv_range,100,50,0),
	     cvScalar(pixelvals[0]+hsv_range, 255, 255, 0),
	     tresh_image);
}
