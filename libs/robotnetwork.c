#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
//#include <sys/time.h>
#include <math.h>
#include "robotnetwork.h"
#include "robotserial.h"


//#define PORT 8888

int sock, i, slen, recv_len;
int port;
struct sockaddr_in si_me, si_other;
//clock_t timer;
//struct timespec spec;
struct timeval timer;

void startTimer(){
  //  timer = clock();
  //clock_gettime(CLOCK_REALTIME, &spec);
  gettimeofday(&timer, NULL);
}

void resetTimer(){
  //  timer = clock();
  //  clock_gettime(CLOCK_REALTIME, &spec);
  gettimeofday(&timer, NULL);
}

long getTimerInMs(){
  struct timeval current;
  gettimeofday(&current, NULL);
  long ms = ((current.tv_sec - timer.tv_sec)*1000)+((current.tv_usec - timer.tv_usec)/1000);
  return ms;
}

int hardTimeoutExceeded(){
  //long ms = getTimerInMs();
  //printf("Time passed: %d\n",ms);
  return (getTimerInMs() >= NET_HARD_TIMEOUT);
}

int softTimeoutExceeded(){
  return (getTimerInMs() >= NET_SOFT_TIMEOUT);
}

void printBinary(char* buf, int len){
  int i, j;
  for (i=0; i<len; i++){
    char byte = buf[i];
    for (j=7; j>=0; j--){
      int bit = (byte >> j)&1;
      if (bit == 1)
	printf("1");
      else
	printf("0");
    }
    printf(" ");
  }
  printf("\n");
}

void closeNetworkWithError(char* s){
  perror(s);
  closeNetwork();
}

int isValidNetInstruction(char* instr, int instrlen){
  if (isValidNetHeader(instr[0])){
    int type = instr[0] >> 4;
    int len = instrlen-1; //header is included in len, we don't want that
    switch (type){
    case NETINSTR_STOP:
    case NETINSTR_CONTACT:
    case NETINSTR_PING:
      if (len == 0)
	return 1;
      break;
    case NETINSTR_BTNS:
      if (len == 1)
	return 1;
      break;
    case NETINSTR_IMGMODE:
      if (len == 1){
	if (instr[1] <= 3)
	  return 1;
      }
    case NETINSTR_DIR:
      if (len == 2)
	return 1;
      break;
    case NETINSTR_MOUSE:
      if (len == 2){
	//X should be between 1-160
	if (instr[1] == 0 || instr[1] > 160)
	  break;
	//Y should be between 1-120
	if (instr[2] == 0 || instr[2] > 120) //Y
	  break;
	return 1;
      }
      break;
    default:
      break;
    }
  }
  return 0;
}

int isValidNetHeader(char header){
  int type = header >> 4;
  int data_bytes = header & 0x0F;
  switch (type){
  case NETINSTR_STOP:
  case NETINSTR_CONTACT:
  case NETINSTR_PING:
    if (data_bytes == 0)
      return 1;
    break;
  case NETINSTR_BTNS:
  case NETINSTR_IMGMODE:
    if (data_bytes == 1)
      return 1;
    break;
  case NETINSTR_DIR:
  case NETINSTR_MOUSE:
    if (data_bytes == 2)
      return 1;
    break;
  default:
    break;
  }
}

void closeNetwork(){
  close(sock);
}

int initNetwork(int port){
  slen = sizeof(si_other);
  char buf[BUFLEN];

  //create a UDP socket
  if ((sock=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    closeNetworkWithError("socket");

  memset((char*) &si_me, 0, sizeof(si_me));

  si_me.sin_family = AF_INET;
  si_me.sin_port = htons(port);
  si_me.sin_addr.s_addr = htonl(INADDR_ANY);

  //bind socket to port
  if(bind(sock, (struct sockaddr*)&si_me, sizeof(si_me))==-1){
    closeNetworkWithError("bind");
    return -1;
  }
}

int getMessage(char* buf){
  return recvfrom(sock,buf,BUFLEN,MSG_DONTWAIT,(struct sockaddr*)&si_other,&slen);
}

int sendMessage(char* buf, int len){
  return sendto(sock,buf,len,MSG_DONTWAIT,(struct sockaddr*)&si_other, slen);
}

int sendImage(IplImage* img){
  return sendMessage(img->imageData, BUFLEN_IMG);
}

void waitForContact(){
  char buf[512];
  int recv_len;
  setRGBLed(LED_R,LED_OFF,LED_OFF);
  while (1){
    if ((recv_len = getMessage(buf)) != -1){
      if (recv_len == 1){
	if ((buf[0] >> 4) == NETINSTR_CONTACT){
	  printf("contact: %d\n",buf[0]);
	  sendMessage(NETINSTR_ACK,1);
	  setRGBLed(LED_OFF,LED_G,LED_OFF);
	  return;
	}
      }
    }
  }
}
