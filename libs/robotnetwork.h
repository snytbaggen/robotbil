#include <opencv/cv.h>

#define NETINSTR_STOP 0
#define NETINSTR_CONTACT 1
#define NETINSTR_DIR 2
#define NETINSTR_MOUSE 3
#define NETINSTR_BTNS 4
#define NETINSTR_PING 5
#define NETINSTR_IMGMODE 6

#define NETINSTR_ACK "K" //Should actually be 'K' but sendMessage complains

#define BUFLEN 16
//#define IMGSIZE 230400
#define BUFLEN_IMG 57600

#define NET_HARD_TIMEOUT 3000
#define NET_SOFT_TIMEOUT NET_HARD_TIMEOUT/2

void printBinary(char* buf, int len);
int initNetwork();
//void sendImage(IplImage* image);
void waitForContact(); //Blocking until contact is established
int getMessage(char* buf);
void closeNetwork();
int sendMessage(char* buf, int len);
int sendImage(IplImage* img);

int isValidNetInstruction(char* instr, int instrlen);
int isValidNetHeader(char instr);

int hardTimeoutExceeded();
int softTimeoutExceeded();
void resetTimer();
void startTimer();
